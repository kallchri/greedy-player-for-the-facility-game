package facilityGame;




public class christosc extends FPlayer{

	static String playerName = "christos";
	static String version = "1.0";
	// Give your personal information in Greek
	static int afm = 56218; // AFM should be in form 5XXXX
	static String firstname = "Αλίκη";
	static String lastname = "Στεφανοπούλου";
	
	// Member variables
	int n; // Number of nodes in the graph


	// Constructor
	public nikosc(EnumPlayer player) {
		// Call the constructor of the parent class
		super(player, playerName, version, afm, firstname, lastname);

	}

	// Initialize Player
	// This method is called before the game starts
	// Each player can override this method
	public void initialize(FacilityGameAPI game) {
		n = game.getN();
		
	}

	public int nextMove(FacilityGameAPI game) {
		int move = -1;
	    int oplm = -1;
	
		
		oplm = game.getMoveLocation().lastElement();
		   
		for (int i = 0; i <n; i++) 
		{
		    
			if (game.getValue(oplm)+game.getValue(i) == 9999 && game.getStatus(i) == EnumFacilityStatus.FREE) {
				   
			    move = i;
			    break;
			}
		 }
	   
	   
		return move;
		
		
	}
}

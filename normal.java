

package facilityGame;

import java.util.Vector;

public class PoppyChr extends FPlayer {
	static String playerName = "alice";
	static String version = "1.0";
	// Give your personal information in Greek
	static int afm = 56218; // AFM should be in form 5XXXX
	static String firstname = "?a????p?";
	static String lastname = "???st?f???d??";

	// Member variables
		int n; // Number of nodes in the graph
		

		// Constructor
		public alikstef (EnumPlayer player) {
			// Call the constructor of the parent class
			super(player, playerName, version, afm, firstname, lastname);
		
		}

		// Initialize Player
		// This method is called before the game starts
		// Each player can override this method
		public void initialize(FacilityGameAPI game) {
			n = game.getN();
		}
		
		public int nextMove(FacilityGameAPI game) {
			int move = -1;
			
	
				// VARIABLES
				//------------------------------------------------------------------------------
				int i = 0;
				int value1 = 0;
				int move1 = 0;
				//------------------------------------------------------------------------------
				// end VARIABLES
				
				// BLOCK 2-IN-A-ROW
				//==============================================================================
				// 1ST CASE
				//------------------------------------------------------------------------------
				while (i < (n - 7))
				{
					if (((game.getStatus(i) == me &&
					   game.getStatus(i + 6) == EnumFacilityStatus.FREE &&
					   game.getValue(i + 6) > value1) ||
					   (game.getStatus(i) == EnumFacilityStatus.FREE &&
					   game.getStatus(i + 6) == me &&
					   game.getValue(i) > value1))
					   &&
					   (game.getStatus(i + 2) == opponent &&
					   game.getStatus(i + 4) == opponent))
					{
						if (game.getStatus(i) == EnumFacilityStatus.FREE)
						{
							value1 = game.getValue(i);
							move1 = i;
						}
						else
						{
							value1 = game.getValue(i + 6);
							move1 = i + 6;
						}
						i = i + 7;
					}
					i++;
				}
				i = 0;
				if (((game.getStatus(n - 7) == me &&
				   game.getStatus(n - 1) == EnumFacilityStatus.FREE &&
				   game.getValue(n - 1) > value1) ||
				   (game.getStatus(n - 7) == EnumFacilityStatus.FREE &&
				   game.getStatus(n - 1) == me &&
				   game.getValue(n - 7) > value1))
				   &&
				   (game.getStatus(n - 5) == opponent &&
				   game.getStatus(n - 3) == opponent))
				{
				   if (game.getStatus(n - 7) == EnumFacilityStatus.FREE)
				   {
					   value1 = game.getValue(n - 7);
					   move1 = n - 7;
				   }
				   else
				   {
					   value1 = game.getValue(n - 1);
					   move1 = n - 1;
				   }
				}
				//------------------------------------------------------------------------------
				// end 1ST CASE
				// 2ND CASE
				//------------------------------------------------------------------------------
				while (i < (n - 8))
				{
					if (((game.getStatus(i) == me &&
					   game.getStatus(i + 7) == EnumFacilityStatus.FREE &&
					   game.getValue(i + 7) > value1) ||
					   (game.getStatus(i) == EnumFacilityStatus.FREE &&
					   game.getStatus(i + 7) == me &&
					   game.getValue(i) > value1))
					   &&
					   ((game.getStatus(i + 2) == opponent || game.getStatus(i + 3) == opponent) &&
					   game.getStatus(i + 5) == opponent))
					{
						if (game.getStatus(i) == EnumFacilityStatus.FREE)
						{
							value1 = game.getValue(i);
							move1 = i;
						}
						else
						{
							value1 = game.getValue(i + 7);
							move1 = i + 7;
						}
						i = i + 8;
					}
					i++;
				}
				i = 0;
				if (((game.getStatus(n - 8) == me &&
				   game.getStatus(n - 1) == EnumFacilityStatus.FREE &&
				   game.getValue(n - 1) > value1) ||
				   (game.getStatus(n - 8) == EnumFacilityStatus.FREE &&
				   game.getStatus(n - 1) == me &&
				   game.getValue(n - 8) > value1))
				   &&
				   ((game.getStatus(n - 5) == opponent || game.getStatus(n - 6) == opponent) &&
				   game.getStatus(n - 3) == opponent))
				{
				   if (game.getStatus(n - 8) == EnumFacilityStatus.FREE)
				   {
					   value1 = game.getValue(n - 8);
					   move1 = n - 8;
				   }
				   else
				   {
					   value1 = game.getValue(n - 1);
					   move1 = n - 1;
				   }
				}
				//------------------------------------------------------------------------------
				// end 2ND CASE
				// 3RD CASE
				//------------------------------------------------------------------------------
				while (i < (n - 9))
				{
					if (((game.getStatus(i) == me &&
					   game.getStatus(i + 8) == EnumFacilityStatus.FREE &&
					   game.getValue(i + 8) > value1) ||
					   (game.getStatus(i) == EnumFacilityStatus.FREE &&
					   game.getStatus(i + 8) == me &&
					   game.getValue(i) > value1))
					   &&
					   (game.getStatus(i + 3) == opponent &&
					   game.getStatus(i + 6) == opponent))
					{
						if (game.getStatus(i) == EnumFacilityStatus.FREE)
						{
							value1 = game.getValue(i);
							move1 = i;
						}
						else
						{
							value1 = game.getValue(i + 8);
							move1 = i + 8;
						}
						i = i + 10;
					}
					i++;
				}
				i = 0;
				if (((game.getStatus(n - 9) == me &&
				   game.getStatus(n - 1) == EnumFacilityStatus.FREE &&
				   game.getValue(n - 1) > value1) ||
				   (game.getStatus(n - 9) == EnumFacilityStatus.FREE &&
				   game.getStatus(n - 1) == me &&
				   game.getValue(n - 9) > value1))
				   &&
				   (game.getStatus(n - 6) == opponent &&
				   game.getStatus(n - 3) == opponent))
				{
				   if (game.getStatus(n - 9) == EnumFacilityStatus.FREE)
				   {
					   value1 = game.getValue(n - 9);
					   move1 = n - 9;
				   }
				   else
				   {
					   value1 = game.getValue(n - 1);
					   move1 = n - 1;
				   }
				}
				//------------------------------------------------------------------------------
				// end 3RD CASE
				//==============================================================================
				// end BLOCK-2-IN-A-ROW
				
				if (move1 != 0)
				{
					move = move1;
				}
				else
				{
					int max = 0;
					int free_max_position = 0;
					for  (i = 0; i < n; i++)
					{
						if ((game.getValue(i) > max) && (game.getStatus(i) == EnumFacilityStatus.FREE))
						{
							max = game.getValue(i);
							free_max_position = i;			
						}
					}
					move = free_max_position;
				}
				

					
			return move;
		}
	}

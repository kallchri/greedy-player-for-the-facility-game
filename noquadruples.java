package facilityGame;

import java.util.Vector;

public class PoppyChr extends FPlayer {
	static String playerName = "alice";
	static String version = "1.0";
	// Give your personal information in Greek
	static int afm = 56218; // AFM should be in form 5XXXX
	static String firstname = "?a????p?";
	static String lastname = "???st?f???d??";

	// Member variables
		int n; // Number of nodes in the graph
		int round;

		// Constructor
		public alikstef (EnumPlayer player) {
			// Call the constructor of the parent class
			super(player, playerName, version, afm, firstname, lastname);
		
		}

		// Initialize Player
		// This method is called before the game starts
		// Each player can override this method
		public void initialize(FacilityGameAPI game) {
			n = game.getN();
			round=1;
		}
		
		public int nextMove(FacilityGameAPI game) {
			int move = -1;
			Vector<Integer> movelocation = game.getMoveLocation(); 
			int index = movelocation.lastElement();
			boolean check = true;
			
			if (round == 1 || round== 3 ) 
			{
				if ( index + 4 <=n )
				{
					if (game.getStatus(index + 4) == EnumFacilityStatus.FREE) 
					{
						move = index + 4 ; 
					}
					else check = false ;
				}
				    else check = false; 
					if (round==3) round = 1;
					else round = 2;
				}
				else
				{
					if(index - 2 >= 0)
					{
						if(game.getStatus(index-2)==EnumFacilityStatus.FREE)
						{
							move = index - 2;
						}
						else check = false;
					}
					else check = false; 
				}
				
				if (check == false)
				{
					for(int i=0; i < n; i++) {
						if (game.getStatus(i) == EnumFacilityStatus.FREE)
						{	
							move = i;
						    round = 1;
						    break;
					}
				}
			}
			
			return move;
		
		}
}

